# Dictaphone

Pour prendre des notes oralement vous aurez besoin :
- d'un logiciel pour enregistrer
- d'un logiciel de transcription

Il se trouve que *Google* propose les deux au travers de son **Google Docs**.
Ce logiciel, disponible pour téléphone portable, permet de composer du texte mais de le faire également par oral. Il suffit de dicter au logiciel qui produit alors le texte correspondant.

Ce texte est au format **Google Docs** mais peut être ensuite exporté au format *docx*, format standard utilisable par la plupart des traitements de texte (**Microsoft Word**, **LibreOffice**, …).

