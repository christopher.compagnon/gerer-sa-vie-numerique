# Prendre des notes avec le client de messagerie

Il faut deux messageries.
L'un, officiel qui récupère l'ensemble des courriels et sur lequel tournent les règles de tri.
Un autre, non public servant seulement pour connecter un client de messagerie afin de  prendre des notes.
Les notes sont prises avec le client puis envoyées vers le compte principal.

Le tri peut être plus facilement automatisé en utilisant des mots-clef dans le sujet.

Avantages : ajout de mot-clé, pièces jointes, photo, liens.
Application cliente connue de tous, pas de nouvel apprentissage.
