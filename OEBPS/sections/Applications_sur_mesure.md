# Créer ses propres applications sur mesure

Développer une application n'est pas à la portée de tous. Ils faut disposer de compétences que beaucoup n'ont pas.
Cependant, les applications modernes ont été largement améliorées afin de tenir compte des besoins avancés des utilisateurs, toujours très prompts à réclamer des options afin d'améliorer leur confort. À tel point que les configurations disponibles permettent de disposer d'une personnalisation rendant chaque usage unique.

Dans un certain sens, ces personnalisations sont l'équivalent d'une interface de développement dans un langage intelligible par l'utilisateur. En choisissant bien l'application et en lui appliquant les bons paramètres, il est possible de "développer" une application sur mesure.

Lorsque l'application native atteint ses limites techniques, il est généralement possible d'étendre ses capacités en l'interconnectant avec d'autres applications.

## Pourquoi créer ses propres applications ?

### La sécurité accrue 

Avec l'arrivée d'Internet, le standard d'une application a évolué. Il est aujourd'hui requis qu'elle puisse se connecter à Internet, au moins pour effectuer ses mises à jour automatiquement.

Cependant, une application connectée signifie généralement un compte pour se connecter et une brèche potentielle de sécurité.

En créant sa propre application à partir de briques connues et maîtrisées, nous limitons les brèches potentielles.

### L'apprentissage plus aisé

Les briques choisies pour bâtir ces usages étant connues de l'utilisateur pour les utiliser auparavant, l'apprentissage est plus aisé car il n'a pase à réapprendre tout depuis le début. Il conserve ses acquis, ses habitudes, son environnement. Plutôt que d'utiliser de nouvelles applications tierces, il conserve ses acquis et utilise les outils à disposition de façon plus efficace.

## Comment créer ses applications ?



