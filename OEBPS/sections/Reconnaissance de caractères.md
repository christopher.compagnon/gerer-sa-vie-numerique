# La reconnaissance de texte

Une image étant bien souvent plus percutante que le texte, pour se souvenir de quelque chose, il peut être simple de prendre une photograhpie plutôt que de prendre des notes.

Le téléphone portable possède un appareil photo. Il suffit de prendre un cliché et de le transférer vers

1. Prendre une photograhie
2. Sélectionner le texte avec **Google Lens**. **Google Lens** est un outil de reconnaissance de texte. Il permet de sélectionner une zone de l'image et d'en extraire le texte.
3. Coller le texte dans un document. ce document peut être un courriel (application de courriel du téléphone), un traitement de texte (**Google Docs**), etc.