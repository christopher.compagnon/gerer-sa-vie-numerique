# Les tâches


## Qu'est-ce qu'une tâche ?

Considérons une tâche comme :

- une action ponctuelle.

*Exemple* : 

- une action dont l'exécution peut s'étaler dans le temps de façon discontinue, en pointillés.

*Exemple* : la réception d'une commande. Vous avez effectué une commande en ligne et, de votre point de vue, cette dernière s'achemine en pointillés. D'abord présente chez le fournisseur durant un certain temps, puis chez le transporteur, elle s'achemine progressivement, de poste en poste, de ville en ville.


## Pourquoi créer une tâche ?

Tout comme un événement, une tâche peut être planifiée. Une fois planifiée, notamment avec une date de début, elle disparaît de l'affichage. Ainsi donc planifiée, elle évite de surcharger le bureau et, par conséquent, diminue l'impression de surchage d'activité.

Créer une tâche, c'est avant tout planifier une exécution à venir en fonction des priorités.

Par exemple, si vous devez téléphoner à une personne le mercredi de la semaine prochaine pour discuter d'un sujet, nul besoin de garder un message dans votre boîte de réception. Une simple tâche programée pour être exécutée le jour en question suffira.

**Remarque** : si cet appel a de fortes chances de durer et que la personne ne sera disponible qu'à un certain moment de la journée, alors vous avez plus intérêt à créer un événement.


## Que transformer en tâche ?

### Un courriel

Rappelez-vous : le courriel sert de point d'entrée. Il convient donc de vider la file d'attente le plus possible. Tout message dans la boîte de réception doit donc, dans l'idéal, devenir autre chose qu'un courriel en attente. Ce message peut donc devenir une tâche.

Il s'agit donc une opération à effectuer à un moment donné.

**Remarque** : la plupart des courriels sont soit des informations à prendre connaissance puis supprimer, sinon de choses à réaliser à un moment ou à un autre, donc une tâche (au sens large) à effectuer. Ainsi donc, la très grande majorité des courriels qui nécessitent un «traitement ultérieur» peuvent être transformés en tâche en premier lieu, avant de dévenir «autre chose» le moment venu.

Dans la plupart des clients de messagerie, il est aisé le transformer un courriel en tâche.

Par exemple, dans **Thunderbird**, un simple clic-droit sur un message donnera accès à un menu contextuel assez fourni.

![Thunderbird : menu contextuel sur un message](../images/Thunderbird-message-popup-menu.png)

Il ne reste plus qu'à choisir **Convertir en > Tâche…** :

![Thunderbird : conversion d'une message en tâche](../images/Thunderbird-convert-into-task1.png)

et le tour est joué.

Il ne reste plus qu'à définir éventuellement une date de début et/ou de fin afin de la planifier puis de gérer l'avancement de l'exécution.

### Une URL

Si l'URL peut devenir un contenu à lire plus tard (voir : Read It Later), elle peut aussi devenir une tâche à exécuter. Il suffit de mettre le lien directement dans la description de la tâche.

Dans certains logiciels, il est aussi possible de créer directement une tâche sous la forme d'un lien.

Par exemple, dans **Thunderbird**, depuis le menu **Joindre > Page web…** :

![Thunderbird : création d'une URL dans une tâche](../images/Thunderbird-task-url1.png)

Puis en définissant le lien directement à partir de la nouvelle fenêtre :

![Thunderbird : définition d'une URL dans une tâche](../images/Thunderbird-task-url2.png)

### Un envoi de lettre/colis suivi/recommandé

Quand vous envoyez une lettre ou un colis suivi/recommandé, vous disposez d'un identifiant afin de suivre le trajet.
Notez cet envoi comme une tâche, avec l'identifiant et éventuellement le lien vers le site du transporteur afin de suivre plus aisément la livraison.

### Une réception de commande en ligne

Tout commme l'envoi de lettre/colis suivi/recommandé, vous pouvez transformer une commande en ligne en une tâche à suivre.
Vous disposez généralement de :

- la date de la commande : elle deviendra la date de début;
- la date de livraison estimée : elle deviendra la date de fin;
- l'identifiant de la commande ou l'URL pour suivre la livraison du colis : il apparaîtra dans la description.


## Astuces

### Utiliser un événement à la place d'une tâche

Dans l'absolu, une tâche n'est qu'un événement simplifié.

Dans certains cas, il peut être plus compliqué de gérer les événements et les tâches séparément. La synchronisation des tâches entre les différentes applications peut être difficile à réaliser (c'est le cas pour **Google Tasks** / **Thunderbird**), ou bien la gestion des tâches est moins flexible ou moins performante qu'un événement (il n'y a pas la possibilité de définir une *date de début* pour une tâche dans **Google Tasks** donc pas possible de planifier pour le futur).

Pour contourner ces limitations, on peut donc créer des tâches à partir d'événements étendus sur la journée.

#### Méthode

Créer un agenda spécialement pour les tâches afin de ne pas polluer les autres événements et pouvoir activer/désactiver la partie des tâches sans toucher au reste.
Pour chaque «tâche» créée et planifiée, la placer dans cet agenda par défaut avec l'option «Événement sur la journée» activée.
Une fois la tâche achevée, la déplacer dans un autre calendrier (au jour de son exécution réelle) ou la supprimer. Si vous voulez la laisser là où elle est, vous devez alors la changer de couleur (si votre application le permet). Vous pouvez aussi créer un autre agenda «tâches achevées» et déplacer la tâche achevée dans cet agenda.
Pour les tâches du passé non exécutées, les replanifier dans le futur.

Ainsi, dans tous les cas, l'agenda des tâches ne doit pas contenir de tâches dans le passé puisqu'elles sont soit dans un autre agenda quand elles ont été exécutées, soit supprimées, soit dans le futur.

#### Avantages

- tâches et événements sont gérés au même endroit, avec les mêmes outils et la même interface sans apprentissage supplémentaire;
- la méthode fonctionne de la même façon quel que soit l'application utilisée (**Google Calendar**, **Thunderbird**, **Microsoft Outlook**) ou le contexte (privé/professionnel);
- plus de limitations sur les synchronisation;
- possibilité de planification grâce à une date de début/fin;
- dans certains cas, possibilité d'ajouter des options complémentaires (lieux, couleurs, pièces-jointes, mise en page, partage entre plusieurs personnes, …);
- conversion d'une tâche en événement plus aisée.

#### Inconvénients

- gestion manuelle des «tâches» dans le passé – si elles n'ont pas été exécutées, il faut les replanifier manuellement;
- obligation de suivre régulièrement les tâches pour faire le ménage et la replanification.



