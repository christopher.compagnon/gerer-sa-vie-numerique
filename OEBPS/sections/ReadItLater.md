# Lire plus tard (Read It Later)

## Depuis l'ordinateur de bureau

## Depuis le téléphone portable

### Client de messagerie

Depuis votre navigateur, allez dans les outils complémentaires de la page affichée et choisissez le partage. Cette option permet d'envoyer le lien de la page consultée vers les réseaux sociaux, des contacts dans les messageries instantanées ou tout simplememnt copier/coller le lien.

Choisissez cette dernière option, puis collez le lien précédemment mémorisé dans un message que vous enverrez à  votre messagerie privée.

### Google Chrome

Si vous disposez d'un appareil *Android*, mettre de côté des articles est assez simple.

Depuis le navigateur **Google Chrome**, ouvrez la page qui vous intéresse et imprimez-la.
Elle sera automatiquement enregistrée au format *PDF*.
Ouvrez-la et ajoutez-la sur **Google Drive**. Il ne vous reste plus qu'à la classer dans son répertoire.

## Firefox

Si vous utilisez Firefox, il existe deux méthodes assez simples :

- l'enregistrement de la page au format *PDF*;
- l'enregistnement de la page dans **Firefox Pocket**.

**Firefox Pocket** permet de mémoriser les URL (pages web, vidéos *YouTube*, etc.) à consulter plus tard, de les organiser par thème, …

