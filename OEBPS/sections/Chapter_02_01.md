# Le téléphone portable comme outil de capture

## Introduction

Le téléphone portable est un ordinateur de poche. Il possède deux grands avantages :
1. Sa portabilité fait qu'il peut être avec nous, partout, tout le temps, et donc disponible quand nous en avons besoin.
2. Il contient un processeur sur lequel on peut installer des applications adaptées. Il peut théoriquement exécuter les mêmes applications que celles d'un ordinateur de bureau, à la différence que la taille imitée de son écran necessite des ajustements adéquats.

Cependant, comme le téléphone portable est un ordinateur de poche, certains cherchent à l'utiliser de la même façon que leur ordinateur de bureau, avec les mêmes applications afin de partager les mêmes données.

C'est là que le bât blesse. Ils cherchent à transformer un outil spécifique en appareil identique à tous les autres – donc interchnageable – alors qu'il faudrait, au contraire, compartimenter les usages et affectuer à chaque machine des usages spécifiques en fonctions de ses caractéristiques propres.

La force du téléphone portable est d'être portable, c'est-à-dire qu'il peut être emporté avec nous, partout, tout le temps. Il dispose de capteurs spéciaux embarqués qui en font un outil de capture efficace pour à peu près tout et n'importe quoi. Il peut :

- prendre des photos;
- prendre des vidéos;
- enregistrer des sons;
- enregistrer des tâches et des événements;
- enregistrer du texte.

Mais aussi :

- faire de la reconnaissance de caractères;
- convertir une dictée en texte.

De façon globale, le téléphone portable est l'outil idéal, plus que les autres, pour servir de point d'entrée, le lien entre la vie physique et la vie numérique.

## Mise en place de la plateforme de capture

Nous l'avons déjà évoqué : le courriel est un message enrichi qui peut contenir toutes sortes de contenus. Il est donc le point d'entrée idéal. De plus, les clients de messagerie sont adaptés et éprouvés à ces usages.

Configurons donc une messagerie pour servir de point d'entrée à la vie numérique.

Pour cela, il vous faut 2 adresses de messagerie :

- une adresse publique qui servira de point d'entrée à toute capture;
- une adresse privée qui servira de file d'attente des messages collectés.

### Pourquoi 2 adresses ?

Une adresse de messagerie est généralement un service offert gratuitement à des utilisateurs. 
Comme c'est gratuit, il est possible d'en avoir plusieurs sans débourser un seul centime.
Cependant, cette gratuitée à généralement un coût caché : « **Si c'est gratuit, c'est vous le produit** ».

Il est donc recommandé de disposer de deux adresses de messagerie (on peut en avoir plus, mais deux est suffisant).

- une première pour disposer d'une adresse publique qui sera exposée, cette dernière ayant une redirection activée pour renvoyer tous les messages entrant vers une adresse privée. Comme elle est publique, elle est exposée à les attaques potentielles. En cas de piratage, comme cette messagerie ne contient aucun message puisqu'ils ont tous été renvoyés vers une autre messagerie, elle ne contient aucune information compromettante. cette messagerie peut donc est placée sur un fournisseur intrusif, c'est sans imporatnce, il ne lui reste plus rien à se mettre sous la dent.
- une seconde, non exposée donc privée, pour rassembler tous les messages et les stocker le temps de leur traitement. Comme cette adresse est privée, elle n'est pas exposée aux attaques. Si l'adresse est gérée par un fournisseur réputé pour son respect de la vie privée, alors le respect de la vie privée est mieux garanti.

De plus : disposer de deux adresses permet de construire des applications spécifiques, chacune dédiée à des usages spécifiques, sur des machines spécifiques. En compartimenant, on simplifie la sécurité et la facilité d'usage.

- **Sécurité** : si votre téléphone portable est perdu et tombe entre de mauvaise mains, alors les informations personnelles ne sont pas toutes éparpillées dans la nature.
- **Facilité d'usage** : 

### L'adresse publique


