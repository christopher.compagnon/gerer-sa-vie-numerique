# Point d'entrée/outils de capture

Les informations doivent pouvoir être capturées à n'importe quel moment.
Mais il est inutile de disposer d'appareil et d'applications partageant systématiquement les fonctions et les données.
Il vaut mieux de concentrer sur un appareil effectuant des tâches spécifiques.
Cela évite d'utiliser un même compte sur plusieurs machines en même temps et de rendre les comptes sous-jacents plus vulnérables aux attaques.

Il faut simplement s'arranger pour que les données ne soient donc pas partagées mais qu'elles ruissellent d'un point d'entrée vers le poste de travail.

Ce poste de travail central permet aussi d'éviter de se disperser. Chaque machine a son usage.

Ainsi, un téléphone mobile permettra de prendre rapidement des notes pour plus tard. Mais il n'est pas question en revanche de les organiser depuis là. Sinon, la surcharge mentale guette, profitant du moindre moment de répit pour de déconcentrer de l'important.
C'est comme mélanger la vie personnelle et la vie professionnelle du simple fait de partager les mêmes outils et les mêmes applications.
Au contraire, il faut compartimenter.

Par sa portabilité, il peut aussi faire office d'appareil photo mais aussi grâce au logiciel de reconnaissance de caractère, il peut extraire un texte d'une photographie, qu'on peut ensuite copier/coller ailleurs.

Il peut aussi jouer le rôle de dictaphone et transcrire le texte prononcé en texte écrit, qu'on peut lui aussi intégrer dans un document pour le traiter plus tard.

Le texte écrit est si optimalisé qu'il prend peu de place au regard des autres formats (photos, audio et vidéo). C'est pourquoi, pour des raisons de place et de facité d'usage ou de ré-usage, il convient de passer par ce format autant que possible, même si le format d'origine était autre.