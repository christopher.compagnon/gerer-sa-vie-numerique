# Qu'est-ce que le "Inbox Zero" ?

La méthode *Inbox Zero* est une méthode de gestion des courriels qui vise à avoir une boîte de réception vide à la fin de chaque journée. Elle a été développée par Merlin Mann, un entrepreneur et auteur américain, en 2007.

La méthode *Inbox Zero* repose sur quatre principes fondamentaux :

- Prioriser les courriels : dès réception d'un e-mail, il faut le classer en fonction de son importance et de son urgence. Les courriels importants et urgents doivent être traités immédiatement, les courriels importants mais non urgents doivent être planifiés, et les courriels non importants doivent être supprimés ou archivés.
- Traiter les courriels immédiatement : si un e-mail peut être traité en quelques minutes, il faut le faire immédiatement, plutôt que de le laisser s'accumuler dans la boîte de réception.
- Planifier les courriels à traiter : pour les courriels importants mais non urgents, il faut les planifier pour un moment où on aura le temps de les traiter.
- Supprimer ou archiver les courriels non importants : les courriels non importants doivent être supprimés ou archivés pour libérer de l'espace dans la boîte de réception.

La méthode *Inbox Zero* peut être un outil très efficace pour gagner en productivité et en efficacité. Elle permet de se concentrer sur les tâches importantes et de réduire le stress lié à la gestion des courriels.

Voici quelques conseils pour mettre en place la méthode *Inbox Zero* :

- Définir des règles de tri : il est important de définir des règles de tri claires pour pouvoir classer les courriels rapidement et facilement. Ces règles peuvent être personnalisées en fonction de vos besoins et de vos priorités.
- Utiliser des filtres : les filtres peuvent être utilisés pour automatiser le tri des courriels. Par exemple, vous pouvez créer un filtre pour envoyer automatiquement tous les courriels promotionnels dans un dossier dédié.
- Définir des plages horaires pour la gestion des courriels : il est important de limiter le temps que vous passez à gérer vos courriels. Définissez des plages horaires spécifiques pendant lesquelles vous traiterez vos courriels, et évitez de les consulter en dehors de ces plages.

La méthode *Inbox Zero* nécessite un peu de discipline, mais elle peut apporter de nombreux avantages à ceux qui la mettent en œuvre.

