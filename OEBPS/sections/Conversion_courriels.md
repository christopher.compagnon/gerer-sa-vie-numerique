# Le courriel comme outil de productivité

## Introduction

Les courriels sont un formidable outil de productivité. Bien qu'ils existent depuis les débuts d'Internet et que certains les considèrent comme des reliquats du passé, ils sont toujours d'actualité car ils ne sont que des morceaux de texte enrichi, comme n'importe quel autre message, à la différence essentielle qu'ils jouissent de l'avantage de reposer sur une architecture asynchrone éprouvée.
Nous répondons donc aux courriels quand nous en avons le temps.

Si certains conseillent de les réduire à leur strict usage, ils peuvent servir à bien d'autres choses. Ils sont le point d'entrée de tout un univers numérique allant de la prise de notes à la constitution d'articles élaborés.

Comme la technologie est ancienne et ancrée dans nos usages, les outils de productivité les utilisent à outrance et de nombreuses améliorations ont été apportées, ce qui en fait aujourd'hui un outil dont nous aurions bien tort de nous priver.
Se priver de cet outil et de ses améliorations, c'est s'obliger à utiliser d'autres outils pour compléter ou compenser. Ces verniers nécessitent des comptes pour de connecter, des interconnexions, un apprentissage de chaque outil…
Deplus, les courriels étant très prisés des entreprises, les messageries ne servent pas à seulement consulter ses messages mais à les intégrer dans la chaîne de productivité. Les messageries privées ont hérités de ces améliorations et permettent à n'importe qui de gérer sa vie numérique de façon productive.

## Conversion

Les outils de productivité reposent essentiellement sur la conversion et l'interconnexion. Un message peut alors être facilement transféré et exploité par une autre application. Ainsi, le courriel comme élément de texte enrichi prend tout son potentiel.

Par exemple, il est facile de convertir un message en tâche ou en événement qui apparaîtra dans le calendrier ou le gestionnaire des tâches (*to do list*).
C'est donc le couple courriel + calendrier qui permet de transformer le point d'entrée en outil puissant.
Aujourd'hui, un simple "courriel" comprend le message, des règles de tri et un calendrier. Et ça change toute la donne.
Avec ces 3 outils, nous pouvons organiser simplement et efficacement notre vie.

L'usage du courriel est donc simple : collecter des informations.
Pour notre cerveau, tout est une information par défaut jusqu'à ce qu'il l'ait encodé afin de savoir quoi en faire.

Pour tout message dans la file d'attente (boîte de réception), il convient avant tout de déterminer le type d'information ainsi que la prioriét – donc le délai.
Une information dont on peut ou doit prendre connaissance immédiatement est mis dans les notes ou le calendrier, sinon dans une application jouant le rôle de file d'attente à lire pour plus tard (*Read It Later*).
Un message devient donc :
- Un événement dans le calendrier;
- une tâche dans le gestionnaire de tâches;
- une note;
- un élément à lire plus tard : peut être sous la forme de fichier dans un répertoire de l'ordinateur, ou une page web à lire plus tard (*Firefox Pocket*, *Instapaper*, etc.)

Idéalement, après avoir traité un message, il doit disparaître de la boîte de réception.




Les messages requérant une action sont transformés en tâche.
Par exemple, une commande peut être transformée en tâche avec le début, la date estimée de livraison, le contenu et le lien vers le suivi de colis.


Les abonnements aux newsletters : la plupart du temps, il est conseillé de de désabonner mais cela requiert parfois du temps improductif s'il n'existe pas de lien direct pour l'opération.
Fort heureusement, les règles de filtrage permettent de les écarter ou supprimer automatiquement. Nul besoin de vous désabonner si vous n'avez pas le temps.


La boîte de réception est la liste des messages à interpréter, à "encoder".
Par défaut, un message est juste un item sans aucun sens. Il convient de lui en donner un.
Il y a :
- l'action : une tâche à exécuter ou à suivre.
- l'événement : une opération ou une réunion s'étalant dans le temps et qui nécessite une opération de notre part pour avancer et qui s'étale dans le temps.
- l'information : une information à prendre en compte mais qui ne requiert rien d'autre de notre part que d'en avoir connaissance. Cette information peut être ponctuelle ou utile pour plus tard, dans ce dernier cas il convient de la mettre de côté.
- le lien : un lien vers une ressource.

### Différences entre tâche et événement

La principale différence entre une tâche et un événement est que **la tâche est une action qui doit être effectuée**, tandis que **l'événement est une occurrence qui se produit à un moment donné**.

Contrairement à la tâche qui peut souvent être divisée en sous-tâches, l'événement est un bloc unique qui ne peut pas être subdivisé. Sinon, il s'agit de plusieurs événements distincts. En plus d'êcre un bloc unique, l'événement nécessite très souvent une unité de lieu car il inclut l'intervention d'autres personnes ou de ressources très particulières.

Une tâche peut être simple ou complexe, et elle peut nécessiter ou non des ressources. Par exemple, une tâche simple peut être de faire la vaisselle, tandis qu'une tâche complexe peut être de développer un nouveau logiciel.

Un événement peut être planifié ou non. Par exemple, un anniversaire est un événement planifié, tandis qu'une panne de courant est un événement non planifié.

Voici un tableau récapitulatif des principales différences entre une tâche et un événement :

|Caractéristique|Tâche|Événement|
|:--------------|:----|:--------|
|Définition|Action qui doit être effectuée|Occurrence qui se produit à un moment donné|
|Complexité|Peut être simple ou complexe|Peut être simple ou complexe|
|Ressources|Peut nécessiter ou non des ressources|Nécessite généralement des ressources|
|Planification|Peut être planifiée ou non|Peut être planifiée ou non|

Voici quelques exemples de tâches :

- Faire la vaisselle
- Préparer un repas
- Rédiger un rapport
- Développer un nouveau logiciel

Voici quelques exemples d'événements :

- Un anniversaire
- Un mariage
- Un concert
- Une panne de courant


Pour chaque message, décider s'il doit :
- être archivé;
- avoir une réponse;
- devenir une tâche ou un événement;
- devenir une note servant à la constitution d'une base de connaissances;
- un élément à consulter plus tard.

Autant que possible : créer des règles de tri/filtrage.
N'oubliez pas non plus qu'il est possible de créer des répertoires virtuels.